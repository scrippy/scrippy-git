FROM alpine:latest
RUN apk add gitolite git openssh openssl shadow bash
COPY ./git /var/lib/git
RUN chown -R git: /var/lib/git
COPY start.sh .
RUN chmod +x ./start.sh
RUN ./start.sh init
RUN usermod --password $(echo 0123456789 | openssl passwd -1 -stdin) git
USER git
RUN /usr/bin/gitolite setup -pk /var/lib/git/scrippy.pub
USER root
ENTRYPOINT ["./start.sh", "start"]
EXPOSE 2201
